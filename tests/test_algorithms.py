
import pytest

from tsp import Place
from tsp.algorithms import AntColony, GreedyTSP, Ant


def test_greedy(tsp_obj):

    g = GreedyTSP(initial=Place(3, 1, 1))

    solution = g.run(tsp_obj)

    assert int(solution['cost']) < 8

def test_ant(tsp_obj):
    h = Ant(initial_state = Place(3, 1, 3))
    solution = h.travel(tsp_obj)
    assert int(solution['cost']) < 2

def test_aoc(tsp_obj):
    colony = AntColony()

    solution = colony.run(tsp_obj)

    assert solution['cost'] == 19
